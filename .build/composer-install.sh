#!/usr/bin/env bash

set -e
SCRIPT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
REPOSITORY_ROOT="$(realpath "${SCRIPT_DIR}/..")"
APPLICATION_ROOT="$(realpath "${SCRIPT_DIR}/../app")"

source "${REPOSITORY_ROOT}/.build/include/composer.sh"

run_docker_composer_application composer install --prefer-dist
