#!/usr/bin/env bash

function run_docker_composer() {

IMAGE_COMPOSER="myapp"
DOCKERFILE_PATH="./docker/php/Dockerfile"
TARGET_STAGE="composer"

  docker build -t $IMAGE_COMPOSER --target $TARGET_STAGE -f $DOCKERFILE_PATH .

	docker run -it --rm \
		--user "$(id -u):$(id -g)" \
		"${COMPOSER_DOCKER_ARGS[@]}" \
		--entrypoint "" \
		--env COMPOSER_AUTH \
		--env "COMPOSER_HOME=/tmp/composer" \
		--volume ~/.composer:/tmp/composer \
		"${IMAGE_COMPOSER}" "$@"
}

function run_docker_composer_application() {
	: "${APPLICATION_ROOT?"Missing APPLICATION_ROOT variable"}"
	[ ! -d "${APPLICATION_ROOT}" ] && (
		echo "Invalid APPLICATION_ROOT value"
		exit 1
	)

	COMPOSER_DOCKER_ARGS=(
		--volume "${APPLICATION_ROOT}:/app/main"
		--workdir "/app/main"
	)
	run_docker_composer "$@"
}
