#!/usr/bin/env bash

set -x
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
APPLICATION_ROOT="$(realpath "${SCRIPT_DIR}/../app")"

"${SCRIPT_DIR}/composer-install.sh"
chmod -R 7777 "${APPLICATION_ROOT}/temp" "${APPLICATION_ROOT}/log"
