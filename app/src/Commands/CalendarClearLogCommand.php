<?php

declare(strict_types=1);

namespace App\Commands;

use App\Services\Calendar\CalendarProcessor;
use App\Services\Calendar\ICalendarLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalendarClearLogCommand extends Command
{
    protected static $defaultName = 'calendar:clearLog';

    public function __construct(private readonly ICalendarLogger $logger)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Clear calendar log');
        $this->logger->deleteLog();
        $output->writeln('Done!');

        return 0;
    }
}