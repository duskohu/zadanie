<?php

declare(strict_types=1);

namespace App\Commands;

use App\Services\Calendar\CalendarProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalendarSyncCommand extends Command
{
    protected static $defaultName = 'calendar:sync';

    public function __construct(private readonly CalendarProcessor $calendarProcessor)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Syncing calendar...');
        $this->calendarProcessor->process();
        $output->writeln('Done!');

        return 0;
    }
}