<?php

declare(strict_types=1);

namespace App\Controls;

interface ITeamFormControlFactory
{
    public function create(): TeamFormControl;
}
