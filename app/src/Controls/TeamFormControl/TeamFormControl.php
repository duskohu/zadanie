<?php

declare(strict_types=1);

namespace App\Controls;


use App\Forms\FormFactory;
use App\Model\Team;
use App\Model\TeamMember;
use App\Model\TeamMemberRepository;
use App\Model\TeamMemberRole;
use App\Model\TeamMembership;
use App\Services\PrefixedTranslatorFactory;
use App\Services\TeamService;
use Contributte\Translation\PrefixedTranslator;
use Contributte\Translation\Wrappers\NotTranslate;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class TeamFormControl extends Control
{
    public array $onAdd = [];

    public array $onEdit = [];

    private ?Team $teamEntity = null;

    private PrefixedTranslator $translator;

    /** @var TeamMemberRole[] */
    private array $rolesList;

    public function __construct(
        private readonly FormFactory            $formFactory,
        private readonly EntityManagerInterface $entityManager,
        private readonly TeamService            $teamService,
        PrefixedTranslatorFactory               $prefixedTranslatorFactory,
    )
    {
        $this->translator = $prefixedTranslatorFactory->createPrefixedTranslator();
    }


    public function setTeamEntity(Team $teamEntity): void
    {
        $this->teamEntity = $teamEntity;
    }


    protected function createComponentForm(): Form
    {
        $form = $this->formFactory->create();

        $form->addText('name', 'teamForm.name.label')
            ->setRequired('teamForm.name.required');

        $members = $form->addContainer('members');

        foreach ($this->getRolesList() as $role) {
            /** @var TeamMemberRepository $teamMemberRepository */
            $teamMemberRepository = $this->entityManager->getRepository(TeamMember::class);
            /** @var TeamMember[] $teamMembers */
            $teamMembers = $teamMemberRepository->getTeamMembersByRoleId($role->getId());

            $membersItems = [];
            foreach ($teamMembers as $member) {
                $membersItems[$member->getId()] = new NotTranslate($member->getFullName());
            }
            $members->addMultiSelect($this->createValidComponentName($role->getRole()->value), 'roles.' . $role->getRole()->value, $membersItems)
                ->setOption(
                    'description',
                    $this->translator->translate('teamForm.members.description', ['min' => $role->getMinCount(), 'max' => $role->getMaxCount()])
                );
        }

        $form->addSubmit('send', 'teamForm.submit');


        if ($this->teamEntity) {
            $membersValues = [];
            foreach ($this->teamEntity->getMemberships() as $membership) {
                $membersValues[$this->createValidComponentName($membership->getRole()->getRole()->value)][] = $membership->getMember()->getId();
            }

            $form->setDefaults([
                'name' => $this->teamEntity->getTeamName(),
                'members' => $membersValues,
            ]);

            $form->onSuccess[] = $this->edit(...);
        } else {
            $form->onSuccess[] = $this->add(...);
        }

        $form->onValidate[] = $this->validateForm(...);

        return $form;
    }


    public function validateForm(Form $form, \stdClass $values): void
    {
        foreach ($this->getRolesList() as $role) {
            $componentName = $this->createValidComponentName($role->getRole()->value);
            $min = $role->getMinCount();
            $max = $role->getMaxCount();
            $translatedRole = $this->translator->translate('roles.' . $role->getRole()->value);

            if (count($values->members->$componentName) < $min) {
                $form->addError(
                    new NotTranslate($this->translator->translate('teamForm.members.minError', ['role' => $translatedRole, 'min' => $min]))
                );
            }

            if (count($values->members->$componentName) > $max) {
                $form->addError(
                    new NotTranslate($this->translator->translate('teamForm.members.maxError', ['role' => $translatedRole, 'max' => $max]))
                );
            }
        }

        $members = array_merge(...array_values((array)$values->members));
        $uniqueIds = array_unique($members);
        if (count($members) !== count($uniqueIds)) {
            $form->addError('teamForm.members.oneMemberError');
        }
    }

    private function add(Form $form, \stdClass $values): void
    {
        try {
            $membersWithRoles = [];
            foreach ($this->getRolesList() as $role) {
                $componentName = $this->createValidComponentName($role->getRole()->value);
                $members = $values->members->$componentName;
                foreach ($members as $member) {
                    $membersWithRoles[$member] = $role->getId();
                }
            }

            $this->teamService->createTeam($values->name, $membersWithRoles);
        } catch (\Exception $e) {
            $form->addError('somethingWentWrong');
            return;
        }

        $this->onAdd($this);
    }

    private function edit(Form $form, \stdClass $values): void
    {
        // try {
        $membersWithRoles = [];
        foreach ($this->getRolesList() as $role) {
            $componentName = $this->createValidComponentName($role->getRole()->value);
            $members = $values->members->$componentName;
            foreach ($members as $member) {
                $membersWithRoles[$member] = $role->getId();
            }
        }

        $this->teamService->editTeam($this->teamEntity, $values->name, $membersWithRoles);
        /* } catch (\Exception $e) {
             $form->addError('somethingWentWrong');
             return;
         }*/

        $this->onEdit($this);
    }

    private function createValidComponentName(string $name): string
    {
        $validName = preg_replace('/[^a-zA-Z0-9]/', '_', $name);
        return trim($validName, '_');
    }

    /**
     * @return TeamMemberRole[]
     */
    private function getRolesList(): array
    {
        if (isset($this->rolesList)) {
            return $this->rolesList;
        }

        return $this->rolesList = $this->entityManager->getRepository(TeamMemberRole::class)->findAll();
    }

    public function render(): void
    {
        $form = $this->getComponent('form');
        $template = $this->getTemplate();
        $template->form = $form;
        $template->render(__DIR__ . '/teamFormControl.latte');
    }
}
