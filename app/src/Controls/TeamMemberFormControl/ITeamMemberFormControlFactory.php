<?php

declare(strict_types=1);

namespace App\Controls;

interface ITeamMemberFormControlFactory
{


    public function create(): TeamMemberFormControl;
}
