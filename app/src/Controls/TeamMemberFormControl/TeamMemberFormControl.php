<?php

declare(strict_types=1);

namespace App\Controls;


use App\Forms\FormFactory;
use App\Model\MemberRole;
use App\Model\RoleRepository;
use App\Model\TeamMember;
use App\Model\TeamMemberRole;
use App\Services\TeamService;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class TeamMemberFormControl extends Control
{
    public array $onAdd = [];

    public array $onEdit = [];

    private ?TeamMember $teamMemberEntity = null;


    public function __construct(
        private readonly FormFactory            $factory,
        private readonly EntityManagerInterface $entityManager,
        private readonly TeamService            $teamService,
    )
    {

    }


    public function setTeamMemberEntity(TeamMember $teamMemberEntity): void
    {
        $this->teamMemberEntity = $teamMemberEntity;
    }


    protected function createComponentForm(): Form
    {
        $form = $this->factory->create();
        $form->addText('name', 'teamMemberForm.name.label')
            ->setRequired('teamMemberForm.name.required');

        $form->addText('surname', 'teamMemberForm.surname.label')
            ->setRequired('teamMemberForm.surname.required');

        /** @var RoleRepository $rolesRepository */
        $rolesRepository = $this->entityManager->getRepository(TeamMemberRole::class);

        $roles = array_map(function ($item) {
            return 'roles.' . $item;
        }, $rolesRepository->getRolesAssoc());
        $form->addMultiSelect('roles', 'teamMemberForm.roles.label', $roles)
            ->setRequired('teamMemberForm.roles.required');

        $form->addSubmit('send', 'teamMemberForm.submit');

        if ($this->teamMemberEntity) {
            $form->setDefaults([
                'name' => $this->teamMemberEntity->getName(),
                'surname' => $this->teamMemberEntity->getSurname(),
                'roles' => array_map(function (MemberRole $role) {
                    return $role->getRole()->getId();
                }, $this->teamMemberEntity->getMemberRoles()->toArray()),
            ]);

            $form->onSuccess[] = $this->edit(...);
        } else {
            $form->onSuccess[] = $this->add(...);
        }

        return $form;
    }


    private function add(Form $form, \stdClass $values): void
    {
        try {
            $this->teamService->createTeamMember($values->name, $values->surname, $values->roles);
        } catch (\Exception $e) {
            $form->addError('somethingWentWrong');
            return;
        }

        $this->onAdd($this);
    }

    private function edit(Form $form, \stdClass $values): void
    {
        try {
            $this->teamService->editTeamMember($this->teamMemberEntity, $values->name, $values->surname, $values->roles);
        } catch (\Exception $e) {
            $form->addError('somethingWentWrong');
            return;
        }

        $this->onEdit($this);
    }


    public function render(): void
    {
        $form = $this->getComponent('form');
        $template = $this->getTemplate();
        $template->form = $form;
        $template->render(__DIR__ . '/teamMemberFormControl.latte');
    }
}
