<?php

declare(strict_types=1);

namespace App\Forms;

use App\Services\PrefixedTranslatorFactory;
use Nette\Application\UI\Form;

final class FormFactory
{
    public function __construct(protected PrefixedTranslatorFactory $prefixedTranslatorFactory)
    {

    }

    public function create(): Form
    {
        $form = new Form;
        $form->setTranslator($this->prefixedTranslatorFactory->createPrefixedTranslator());
        return $form;
    }
}
