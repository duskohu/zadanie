<?php

declare(strict_types=1);


namespace App\Forms;

use App\Model\User;
use App\Model\UserRepository;
use App\Services\PrefixedTranslatorFactory;
use Contributte\Translation\PrefixedTranslator;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

final class RegisterFormFactory
{

    private PrefixedTranslator $translator;

    public function __construct(
        private readonly int                    $passwordMinLength,
        private readonly FormFactory            $factory,
        private readonly EntityManagerInterface $entityManager,
        private readonly Passwords              $passwords,
        PrefixedTranslatorFactory               $prefixedTranslatorFactory,
    )
    {
        $this->translator = $prefixedTranslatorFactory->createPrefixedTranslator();
    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addEmail('email', 'registerForm.email.label')
            ->setRequired('registerForm.email.required');

        $form->addPassword('password', 'registerForm.password.label')
            ->setOption(
                'description',
                $this->translator->translate('registerForm.password.description', ['chars' => $this->passwordMinLength])
            )
            ->setRequired('registerForm.password.required')
            ->addRule($form::MIN_LENGTH, null, $this->passwordMinLength);

        $form->addSubmit('send', 'registerForm.submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            try {
                /** @var UserRepository $userRepository */
                $userRepository = $this->entityManager->getRepository(User::class);
                $userRepository->addUser($values->email, $this->passwords->hash($values->password));
            } catch (UniqueConstraintViolationException $e) {
                $form->addError('registerForm.emailError');
                return;
            } catch (\Exception $e) {
                $form->addError('somethingWentWrong');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}
