<?php

declare(strict_types=1);


namespace App\Forms;

use App\Model\CalendarEvent;
use App\Services\EventEmailShare;
use Nette\Application\UI\Form;

final class ShareFormFactory
{

    public function __construct(
        private readonly FormFactory     $factory,
        private readonly EventEmailShare $eventEmailShare,
    )
    {
    }

    public function create(CalendarEvent $calendarEvent, callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $elementPrototype = $form->getElementPrototype();
        $elementPrototype->class[] = 'ajax';


        $form->addEmail('email', 'shareForm.email.label')
            ->setRequired('shareForm.email.required');

        $form->addSubmit('send', 'shareForm.submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess, $calendarEvent): void {
            try {
                $this->eventEmailShare->share($values->email, $calendarEvent);
            } catch (\Exception $e) {
                $form->addError('somethingWentWrong');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}
