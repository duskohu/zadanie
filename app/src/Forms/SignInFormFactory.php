<?php

declare(strict_types=1);


namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

final class SignInFormFactory
{

    public function __construct(
        private readonly FormFactory $factory,
        private readonly User        $user,
    )
    {

    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addEmail('email', 'signInForm.email.label')
            ->setRequired('signInForm.email.required');

        $form->addPassword('password', 'signInForm.password.label')
            ->setRequired('signInForm.password.required');

        $form->addCheckbox('remember', 'signInForm.remember.label');

        $form->addSubmit('send', 'signInForm.submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            try {
                $this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
                $this->user->login($values->email, $values->password);
            } catch (Nette\Security\AuthenticationException $e) {
                $form->addError('signInForm.loginError');
                return;
            } catch (\Exception $e) {
                $form->addError('somethingWentWrong');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}
