<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_events")
 */
class CalendarEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="string") */
    private string $summary;

    /** @ORM\Column(type="string") */
    private string $dtstart;

    /** @ORM\Column(type="string") */
    private string $dtend;

    /** @ORM\Column(type="string") */
    private string $uid;

    /** @ORM\Column(type="string") */
    private string $lastModified;

    public function __construct(
        string $uid,
        string $summary,
        string $dtstart,
        string $dtend,
        string $lastModified,
    )
    {
        $this->uid = $uid;
        $this->summary = $summary;
        $this->dtstart = $dtstart;
        $this->dtend = $dtend;
        $this->lastModified = $lastModified;
    }

    public function getId(): int
    {
        return $this->id;
    }


    public function getSummary(): string
    {
        return $this->summary;
    }

    public function getDtstart(): \DateTime
    {
        return new \DateTime($this->dtstart);
    }

    public function getDtend(): \DateTime
    {
        return new \DateTime($this->dtend);
    }

}
