<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\MemberRoleRepository")
 * @ORM\Table(name="member_roles")
 */
class MemberRole
{

    /**
     * @ORM\ManyToOne(targetEntity="TeamMember", inversedBy="memberRoles")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id")
     */
    private TeamMember $teamMember;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="TeamMemberRole")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private TeamMemberRole $role;

    public function setTeamMember(TeamMember $teamMember): void
    {
        $this->teamMember = $teamMember;
    }

    public function setRole(TeamMemberRole $role): void
    {
        $this->role = $role;
    }

    public function getRole(): TeamMemberRole
    {
        return $this->role;
    }

}