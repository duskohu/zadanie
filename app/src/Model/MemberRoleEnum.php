<?php

declare(strict_types=1);

namespace App\Model;

enum MemberRoleEnum: string
{
    case RACER = 'racer';
    case TECHNICIAN = 'technician';
    case MANAGER = 'manager';
    case CO_DRIVER = 'co-driver';
    case PHOTOGRAPHER = 'photographer';
}
