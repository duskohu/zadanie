<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{

    /**
     * @return array<int, string>
     */
    public function getRolesAssoc(): array
    {
        $qb = $this->createQueryBuilder('r');
        $qb->select('r.id, r.roleName');
        $query = $qb->getQuery();
        $result = $query->getResult();
        $roles = [];
        foreach ($result as $role) {
            $roles[$role['id']] = $role['roleName'];
        }
        return $roles;
    }

}