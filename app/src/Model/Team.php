<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="teams")
 */
class Team
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="string") */
    private string $teamName;

    /**
     * @ORM\OneToMany(targetEntity="TeamMembership", mappedBy="team", cascade={"persist", "remove"})
     * @var TeamMembership[]
     */
    private $memberships;

    public function __construct(string $teamName)
    {
        $this->teamName = $teamName;
        $this->memberships = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTeamName(): string
    {
        return $this->teamName;
    }


    /**
     * @return ArrayCollection|TeamMembership[]
     */
    public function getMemberships()
    {
        return $this->memberships;
    }

    public function setTeamName(string $teamName): void
    {
        $this->teamName = $teamName;
    }

    public function removeMembership(TeamMembership $membership): void
    {
        if ($this->memberships->contains($membership)) {
            $this->memberships->removeElement($membership);
        }
    }

}
