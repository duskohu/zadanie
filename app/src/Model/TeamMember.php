<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\TeamMemberRepository")
 * @ORM\Table(name="team_members")
 */
class TeamMember
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $surname;

    /**
     * @ORM\OneToMany(targetEntity="MemberRole", mappedBy="teamMember", cascade={"persist", "remove"})
     * @var MemberRole[]
     */
    private $memberRoles;

    public function getId(): int
    {
        return $this->id;
    }

    public function __construct(string $name, string $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->memberRoles = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|MemberRole[]
     */
    public function getMemberRoles()
    {
        return $this->memberRoles;
    }

    public function removeMemberRole(MemberRole $memberRole): void
    {
        if ($this->memberRoles->contains($memberRole)) {
            $this->memberRoles->removeElement($memberRole);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getFullName(): string
    {
        return $this->getSurname() . ' ' . $this->getName();
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }


}