<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\EntityRepository;

class TeamMemberRepository extends EntityRepository
{

    public function getTeamMembersByRoleId(int $roleId): array
    {
        $qb = $this->createQueryBuilder('tm');
        $qb->select('tm');
        $qb->leftJoin('tm.memberRoles', 'mr')
            ->leftJoin('mr.role', 'r')
            ->where('r.id = :roleId')
            ->setParameter('roleId', $roleId);

        return $qb->getQuery()->getResult();
    }
}