<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="App\Model\RoleRepository")
 */
class TeamMemberRole
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $roleName;

    /**
     * @ORM\Column(type="integer")
     */
    private int $minCount;

    /**
     * @ORM\Column(type="integer")
     */
    private int $maxCount;

    public function getId(): int
    {
        return $this->id;
    }


    public function getRole(): MemberRoleEnum
    {
        return MemberRoleEnum::from($this->roleName);
    }

    public function getMinCount(): int
    {
        return $this->minCount;
    }

    public function getMaxCount(): int
    {
        return $this->maxCount;
    }


}