<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="team_memberships")
 */
class TeamMembership
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="memberships")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private Team $team;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="TeamMember")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id")
     */
    private TeamMember $member;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="TeamMemberRole")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private TeamMemberRole $role;


    public function setMember(TeamMember $member): void
    {
        $this->member = $member;
    }

    public function setTeam(Team $team): void
    {
        $this->team = $team;
    }

    public function setRole(TeamMemberRole $role): void
    {
        $this->role = $role;
    }

    public function getRole(): TeamMemberRole
    {
        return $this->role;
    }


    public function getMember(): TeamMember
    {
        return $this->member;
    }


}
