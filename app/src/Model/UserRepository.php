<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\EntityRepository;
use Nette\Utils\Validators;

class UserRepository extends EntityRepository
{
    public function addUser(string $email, string $password): void
    {
        Validators::assert($email, 'email');

        $user = new User($email, $password);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

}