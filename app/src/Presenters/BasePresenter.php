<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Services\PrefixedTranslatorFactory;
use Contributte\Translation\PrefixedTranslator;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{

    /** @var PrefixedTranslatorFactory @inject */
    public PrefixedTranslatorFactory $prefixedTranslatorFactory;

    /** @persistent */
    public string $locale;

    protected PrefixedTranslator $prefixedTranslator;

    protected function startup()
    {
        $this->prefixedTranslator = $this->prefixedTranslatorFactory->createPrefixedTranslator();
        $this->template->setTranslator($this->prefixedTranslator);
        $this->template->locale = $this->locale;
        parent::startup();
    }


}
