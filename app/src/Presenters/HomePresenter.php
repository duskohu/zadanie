<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Forms\ShareFormFactory;
use App\Model\CalendarEvent;
use App\Services\Calendar\CalendarProcessor;
use App\Services\CsvResponse;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;

final class HomePresenter extends BasePresenter
{

    private ?CalendarEvent $calendarEvent = null;

    public function __construct(
        private readonly CalendarProcessor      $calendarProcessor,
        private readonly EntityManagerInterface $entityManager,
        private readonly ShareFormFactory       $shareFormFactory
    )
    {
        parent::__construct();
    }

    public function startup()
    {
        parent::startup();
        $template = $this->getTemplate();
        $template->showModal = false;
    }


    public function renderDefault(): void
    {
        $events = $this->entityManager->getRepository(CalendarEvent::class)->findAll();
        $template = $this->getTemplate();
        $template->events = $events;
    }

    public function handleSyncCalendar(): void
    {
        try {
            $this->calendarProcessor->process();
            $this->flashMessage($this->prefixedTranslator->translate('syncSuccess'), 'success');
        } catch (\Throwable $e) {
            $this->flashMessage($this->prefixedTranslator->translate('somethingWentWrong'), 'danger');
        }

        if ($this->isAjax()) {
            $this->redrawControl();
        } else {
            $this->redirect('default');
        }
    }

    public function handleExportCalendar(): void
    {
        try {
            $data = [];

            $events = $this->entityManager->getRepository(CalendarEvent::class)->findAll();
            foreach ($events as $event) {
                $data[] = [
                    'summary' => $event->getSummary(),
                    'dtstart' => $event->getDtstart()->format('d.m.Y'),
                    'dtend' => $event->getDtend()->format('d.m.Y'),
                ];
            }

            $response = new CsvResponse($data, 'calendar.csv');
        } catch (\Throwable $e) {
            $this->flashMessage($this->prefixedTranslator->translate('somethingWentWrong'), 'danger');
            $this->redirect('default');
        }

        $this->sendResponse($response);
    }

    public function actionShare(int $id): void
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->flashMessage($this->prefixedTranslator->translate('notLoggedIn'), 'danger');
            $this->redirect('Sign:');
        }

        $this->calendarEvent = $this->entityManager->getRepository(CalendarEvent::class)->find($id);
        if ($this->calendarEvent === null) {
            throw new BadRequestException();
        }
        $template = $this->getTemplate();
        $template->showModal = true;
        $template->summary = $this->calendarEvent->getSummary();

        if ($this->isAjax()) {
            $this->redrawControl('modal');
        } else {
            $this->redirect('default');
        }
        $this->setView('default');
    }

    protected function createComponentShareForm(): Form
    {
        if ($this->calendarEvent === null) {
            throw new BadRequestException();
        }

        return $this->shareFormFactory->create(
            $this->calendarEvent,
            function (): void {
                $this->template->showModal = false;

                $this->flashMessage($this->prefixedTranslator->translate('shareSuccess'), 'success');
                if ($this->isAjax()) {
                    $this->payload->closeModal = true;
                    $this->redrawControl('modal');
                    $this->redrawControl('flashes');
                } else {
                    $this->redirect('default');
                }
            });
    }

}
