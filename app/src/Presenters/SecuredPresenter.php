<?php

declare(strict_types=1);

namespace App\Presenters;

abstract class SecuredPresenter extends BasePresenter
{
    public function startup(): void
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->flashMessage($this->prefixedTranslator->translate('notLoggedIn'), 'danger');
            $this->redirect('Sign:');
        }
    }

}
