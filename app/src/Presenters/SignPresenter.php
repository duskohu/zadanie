<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\RegisterFormFactory;
use App\Forms\SignInFormFactory;
use Nette\Application\UI\Form;


final class SignPresenter extends BasePresenter
{
    /**
     * @Persistent
     */
    public string $backlink = '';

    public function __construct(
        private readonly SignInFormFactory   $signInFactory,
        private readonly RegisterFormFactory $registerFormFactory
    )
    {
        parent::__construct();
    }

    public function actionDefault(): void
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->flashMessage($this->prefixedTranslator->translate('loggedIn'), 'danger');
            $this->redirect('Home:');
        }
    }

    public function actionOut(): void
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->flashMessage($this->prefixedTranslator->translate('notLoggedIn'), 'danger');
            $this->redirect('Sign:default');
        } else {
            $this->getUser()->logout();
            $this->flashMessage($this->prefixedTranslator->translate('beenSignedOut'), 'success');
            $this->redirect('Sign:default');
        }
    }

    public function actionRegister(): void
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->flashMessage($this->prefixedTranslator->translate('loggedIn'), 'danger');
            $this->redirect('Home:');
        }
    }

    protected function createComponentSignInForm(): Form
    {
        return $this->signInFactory->create(
            function (): void {
                $this->restoreRequest($this->backlink);
                $this->flashMessage($this->prefixedTranslator->translate('successLoggedIn'), 'success');
                $this->redirect('Home:');
            });
    }

    protected function createComponentRegisterForm(): Form
    {
        return $this->registerFormFactory->create(
            function (): void {
                $this->flashMessage($this->prefixedTranslator->translate('successRegistered'), 'success');
                $this->redirect('Home:');
            });
    }
}
