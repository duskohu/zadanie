<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Controls\ITeamMemberFormControlFactory;
use App\Controls\TeamMemberFormControl;
use App\Model\MemberRole;
use App\Model\TeamMember;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Nette\Application\BadRequestException;

final class TeamMemberPresenter extends SecuredPresenter
{
    private ?TeamMember $teamMemberEntity;

    public function __construct(
        private readonly EntityManagerInterface        $entityManager,
        private readonly ITeamMemberFormControlFactory $teamMemberFormControlFactory,
    )
    {
        parent::__construct();
    }

    public function renderDefault(): void
    {
        $template = $this->getTemplate();
        $template->teamMembers = $this->entityManager->getRepository(TeamMember::class)->findAll();;
        $template->filterRoles = function (PersistentCollection $rolesCollection): string {
            $data = [];

            /** @var MemberRole[] $memberRoles */
            $memberRoles = $rolesCollection->getValues();
            foreach ($memberRoles as $memberRole) {
                $data[] = $this->prefixedTranslator->translate('roles.' . $memberRole->getRole()->getRole()->value);
            }
            return implode(', ', $data);
        };
    }

    public function actionEdit(int $id): void
    {
        $this->teamMemberEntity = $this->entityManager->getRepository(TeamMember::class)->find($id);
        if ($this->teamMemberEntity === null) {
            throw new BadRequestException();
        }
    }

    public function actionDelete(int $id): void
    {
        $teamMemberEntity = $this->entityManager->getRepository(TeamMember::class)->find($id);
        if ($teamMemberEntity === null) {
            throw new BadRequestException();
        }

        try {
            $this->entityManager->remove($teamMemberEntity);
            $this->entityManager->flush();
            $this->flashMessage($this->prefixedTranslator->translate('teamMemberDeleted'), 'success');
        } catch (\Exception $e) {
            $this->flashMessage($this->prefixedTranslator->translate('somethingWentWrong'), 'danger');
        }

        $this->redirect('default');
    }

    protected function createComponentTeamMemberAdd(): TeamMemberFormControl
    {
        $control = $this->teamMemberFormControlFactory->create();

        $control->onAdd[] = function (TeamMemberFormControl $control): void {
            $this->flashMessage($this->prefixedTranslator->translate('teamMemberAdded'), 'success');
            $this->redirect('this');
        };

        return $control;
    }

    protected function createComponentTeamMemberEdit(): TeamMemberFormControl
    {
        $control = $this->teamMemberFormControlFactory->create();
        $control->setTeamMemberEntity($this->teamMemberEntity);

        $control->onEdit[] = function (TeamMemberFormControl $control): void {
            $this->flashMessage($this->prefixedTranslator->translate('teamMemberEdited'), 'success');
            $this->redirect('this');
        };

        return $control;
    }

}