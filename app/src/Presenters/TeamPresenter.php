<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Controls\ITeamFormControlFactory;
use App\Controls\TeamFormControl;
use App\Model\Team;
use App\Model\TeamMembership;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Nette\Application\BadRequestException;

class TeamPresenter extends SecuredPresenter
{

    private ?Team $teamEntity;

    public function __construct(
        private readonly EntityManagerInterface  $entityManager,
        private readonly ITeamFormControlFactory $teamFormControlFactory
    )
    {
        parent::__construct();
    }

    public function renderDefault(): void
    {
        $this->template->teams = $this->entityManager->getRepository(Team::class)->findAll();
        $this->template->filterMemberships = function (PersistentCollection $membershipCollection): string {
            $data = [];

            /** @var TeamMembership[] $memberships */
            $memberships = $membershipCollection->getValues();
            foreach ($memberships as $membership) {
                $member = $membership->getMember();
                $data[] = $member->getFullName();
            }

            return implode('<br>', $data);
        };
    }

    public function actionEdit(int $id): void
    {
        $this->teamEntity = $this->entityManager->getRepository(Team::class)->find($id);
        if ($this->teamEntity === null) {
            throw new BadRequestException();
        }
    }

    public function actionDelete(int $id): void
    {
        $teamEntity = $this->entityManager->getRepository(Team::class)->find($id);
        if ($teamEntity === null) {
            throw new BadRequestException();
        }

        try {
            $this->entityManager->remove($teamEntity);
            $this->entityManager->flush();
            $this->flashMessage($this->prefixedTranslator->translate('teamDeleted'), 'success');
        } catch (\Exception $e) {
            $this->flashMessage($this->prefixedTranslator->translate('somethingWentWrong'), 'danger');
        }

        $this->redirect('default');
    }

    protected function createComponentTeamAdd(): TeamFormControl
    {
        $control = $this->teamFormControlFactory->create();

        $control->onAdd[] = function (TeamFormControl $control): void {
            $this->flashMessage($this->prefixedTranslator->translate('teamAdded'), 'success');
            $this->redirect('this');
        };

        return $control;
    }

    protected function createComponentTeamEdit(): TeamFormControl
    {
        $control = $this->teamFormControlFactory->create();
        $control->setTeamEntity($this->teamEntity);

        $control->onEdit[] = function (TeamFormControl $control): void {
            $this->flashMessage($this->prefixedTranslator->translate('teamEdited'), 'success');
            $this->redirect('this');
        };

        return $control;
    }

}