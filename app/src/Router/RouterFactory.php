<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
    use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
        $router = new RouteList;

        $router->addRoute('[<locale=en cs|en>/]sign', 'Sign:default');
        $router->addRoute('[<locale=en cs|en>/]sign-out', 'Sign:out');
        $router->addRoute('[<locale=en cs|en>/]register', 'Sign:register');

        $router->addRoute('[<locale=en cs|en>/]<presenter>/<action>[/<id>]', 'Home:default');
        return $router;
    }
}
