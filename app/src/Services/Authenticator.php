<?php

declare(strict_types=1);

namespace App\Services;

use App\Model\User;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\Security\SimpleIdentity;


final class Authenticator implements \Nette\Security\Authenticator
{
    public function __construct(
        private readonly Passwords              $passwords,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    public function authenticate(string $user, string $password): SimpleIdentity
    {
        $userRepository = $this->entityManager->getRepository(User::class);

        /** @var User $userEntity */
        $userEntity = $userRepository->findOneBy(['email' => $user]);

        if (!$userEntity) {
            throw new AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

        } elseif (!$this->passwords->verify($password, $userEntity->getPassword())) {
            throw new AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        } elseif ($this->passwords->needsRehash($userEntity->getPassword())) {
            $userEntity->setPassword($this->passwords->hash($password));
            $this->entityManager->flush();
        }

        $data = [
            'email' => $userEntity->getEmail(),
        ];
        return new SimpleIdentity($userEntity->getId(), [], $data);
    }
}
