<?php

declare(strict_types=1);


namespace App\Services\Calendar;


use ICal\ICal;

class CalendarDataImporter implements ICalendarDataImporter
{

    public function __construct(
        private readonly ICalendarDataStorage $calendarDataStorage,
    )
    {
    }

    public function import(ICal $ical): void
    {
        $this->calendarDataStorage->save($ical);
    }

}