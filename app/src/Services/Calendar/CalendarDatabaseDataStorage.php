<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use App\Model\CalendarEvent;
use Doctrine\ORM\EntityManagerInterface;
use ICal\Event;
use ICal\ICal;

class CalendarDatabaseDataStorage implements ICalendarDataStorage
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ICalendarLogger        $logger,
    )
    {

    }


    public function save(ICal $ical): void
    {
        $events = $ical->events();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('e.uid', 'e.lastModified')
            ->from(CalendarEvent::class, 'e');
        $query = $qb->getQuery();
        $existingEvents = array_column($query->getResult(), 'lastModified', 'uid');

        $importedUids = [];
        $newEvents = [];
        $updatedEvents = [];
        foreach ($events as $event) {
            $uid = $event->uid;
            $importedUids[] = $uid;

            if (array_key_exists($uid, $existingEvents)) {
                if ($existingEvents[$uid] !== $event->last_modified) {
                    $updatedEvents[] = $uid;
                    $this->updateEvent($event);
                }
            } else {
                $this->createEvent($event);
                $newEvents[] = $uid;
            }
        }

        $this->deleteEvents($importedUids);

        $this->entityManager->flush();
        $this->logger->logUpdateEvents($updatedEvents);
        $this->logger->logNewEvents($newEvents);
    }

    private function createEvent(Event $event): void
    {
        $eventEntity = new CalendarEvent(
            $event->uid,
            $event->summary,
            $event->dtstart,
            $event->dtend,
            $event->last_modified,
        );

        $this->entityManager->persist($eventEntity);
    }

    private function updateEvent(Event $event): void
    {
        $query = $this->entityManager->createQuery('UPDATE ' . CalendarEvent::class . ' e SET e.summary = :summary, e.dtstart = :dtstart, e.dtend = :dtend, e.lastModified = :lastModified WHERE e.uid = :uid');
        $query->setParameter('uid', $event->uid);
        $query->setParameter('summary', $event->summary);
        $query->setParameter('dtstart', $event->dtstart);
        $query->setParameter('dtend', $event->dtend);
        $query->setParameter('lastModified', $event->last_modified);
        $query->execute();
    }

    private function deleteEvents(array $importedUids): void
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('e.uid')
            ->from(CalendarEvent::class, 'e')
            ->where('e.uid NOT IN (:uids)');
        $query = $qb->getQuery();
        $query->setParameter('uids', $importedUids);
        $uidsForDelete = array_column($query->getResult(), 'uid');

        $query = $this->entityManager->createQuery('DELETE FROM ' . CalendarEvent::class . ' e WHERE e.uid IN (:uids)');
        $query->setParameter('uids', $uidsForDelete);
        $query->execute();
        $this->logger->logDeleteEvents($uidsForDelete);
    }
}
