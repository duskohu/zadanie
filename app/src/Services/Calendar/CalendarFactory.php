<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use ICal\ICal;

class CalendarFactory implements ICalendarFactory
{

    public function create(ICalendarSourceStorage $calendarStorage): ICal
    {
        $data = $calendarStorage->getCalendarData();
        $ical = new ICal(false, array('defaultSpan' => 0));
        $ical->initString($data);
        return $ical;
    }

}