<?php

declare(strict_types=1);

namespace App\Services\Calendar;

class CalendarProcessor
{

    public function __construct(
        private readonly ICalendarSourceStorage $calendarSource,
        private readonly ICalendarDataImporter  $calendarDataImporter,
        private readonly ICalendarFactory       $calendarFactory
    )
    {
    }

    public function process(): void
    {
        $calendar = $this->calendarFactory->create($this->calendarSource);
        $this->calendarDataImporter->import($calendar);
    }
}