<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use Tracy\ILogger;

class CalendarTracyLogger implements ICalendarLogger
{
    private const LOG_LEVEL = 'calendar';

    public function __construct(
        private readonly string  $logDir,
        private readonly ILogger $logger
    )
    {
    }


    public function logUpdateEvents(array $ids): void
    {
        if (count($ids) > 0) {
            $this->logger->log('Updated events events' . json_encode($ids), self::LOG_LEVEL);
        }
    }

    public function logNewEvents(array $ids): void
    {
        if (count($ids) > 0) {
            $this->logger->log('New events' . json_encode($ids), self::LOG_LEVEL);
        }
    }

    public function logDeleteEvents(array $ids): void
    {
        if (count($ids) > 0) {
            $this->logger->log('Deleted events' . json_encode($ids), self::LOG_LEVEL);
        }
    }

    public function deleteLog(): void
    {
        unlink($this->logDir . '/' . self::LOG_LEVEL . '.log');
    }

}