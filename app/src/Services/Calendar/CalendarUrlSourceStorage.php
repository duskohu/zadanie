<?php

declare(strict_types=1);

namespace App\Services\Calendar;

class CalendarUrlSourceStorage implements ICalendarSourceStorage
{

    public function __construct(private readonly string $calendarUrl)
    {
    }

    public function getCalendarData(): string
    {
        $icsData = file_get_contents($this->calendarUrl);
        if ($icsData === false) {
            throw new \Exception('Unable to download calendar file.');
        }

        return $icsData;
    }

}