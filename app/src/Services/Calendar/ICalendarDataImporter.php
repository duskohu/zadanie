<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use ICal\ICal;

interface ICalendarDataImporter
{

    public function import(ICal $ical): void;

}