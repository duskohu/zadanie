<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use ICal\ICal;

interface ICalendarDataStorage
{
    public function save(ICal $ical): void;

}