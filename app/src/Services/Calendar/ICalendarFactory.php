<?php

declare(strict_types=1);

namespace App\Services\Calendar;

use ICal\ICal;

interface ICalendarFactory
{
    public function create(ICalendarSourceStorage $calendarStorage): ICal;

}