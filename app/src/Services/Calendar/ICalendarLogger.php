<?php

declare(strict_types=1);

namespace App\Services\Calendar;

interface ICalendarLogger
{
    public function logUpdateEvents(array $ids): void;

    public function logNewEvents(array $ids): void;

    public function logDeleteEvents(array $ids): void;

    public function deleteLog(): void;

}