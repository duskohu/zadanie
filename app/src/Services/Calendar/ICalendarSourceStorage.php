<?php

declare(strict_types=1);

namespace App\Services\Calendar;

interface ICalendarSourceStorage
{
    public function getCalendarData(): string;

}