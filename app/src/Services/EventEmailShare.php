<?php

declare(strict_types=1);

namespace App\Services;

use App\Model\CalendarEvent;
use Contributte\Translation\PrefixedTranslator;
use Nette\Mail\Mailer;
use Nette\Mail\Message;
use Nette\Utils\Validators;

class EventEmailShare
{
    private PrefixedTranslator $prefixedTranslator;

    public function __construct(
        PrefixedTranslatorFactory $prefixedTranslatorFactory,
        private readonly Mailer   $sendmailMailer,
    )
    {
        $this->prefixedTranslator = $prefixedTranslatorFactory->createPrefixedTranslator();
    }

    public function share(string $email, CalendarEvent $event): void
    {
        if (!Validators::isEmail($email)) {
            throw new \Exception('Invalid email');
        }

        $mail = new Message();
        $mail->setFrom($email)
            ->addTo($email)
            ->setSubject($this->prefixedTranslator->translate('eventEmailShare.subject'))
            ->setBody($this->prefixedTranslator->translate(
                'eventEmailShare.body',
                [
                    'summary' => $event->getSummary(),
                    'start' => $event->getDtstart()->format('d.m.Y')
                ])
            );
        bdump($mail);
        //$this->sendmailMailer->send($mail);
    }
}