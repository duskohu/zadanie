<?php

declare(strict_types=1);

namespace App\Services;

use Contributte\Translation\PrefixedTranslator;
use Contributte\Translation\Translator;

final class PrefixedTranslatorFactory
{

    public function __construct(
        private readonly Translator $translator
    )
    {

    }

    public function createPrefixedTranslator(string $prefix = 'messages'): PrefixedTranslator
    {
        return $this->translator->createPrefixedTranslator($prefix);
    }

}