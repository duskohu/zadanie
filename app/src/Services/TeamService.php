<?php

declare(strict_types=1);

namespace App\Services;

use App\Model\MemberRole;
use App\Model\TeamMemberRole;
use App\Model\Team;
use App\Model\TeamMember;
use App\Model\TeamMembership;
use Doctrine\ORM\EntityManagerInterface;

class TeamService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {

    }

    public function createTeamMember(string $name, string $surname, array $roleIds): void
    {
        try {
            $this->entityManager->beginTransaction();

            $teamMember = new TeamMember($name, $surname);
            $this->entityManager->persist($teamMember);

            $this->addNewRoles($teamMember, $roleIds);

            $this->entityManager->persist($teamMember);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    public function editTeamMember(TeamMember $teamMember, string $name, string $surname, array $roleIds): void
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->lock($teamMember, \Doctrine\DBAL\LockMode::PESSIMISTIC_WRITE);

            $teamMember->setName($name);
            $teamMember->setSurname($surname);

            $memberRoles = $teamMember->getMemberRoles();
            foreach ($memberRoles as $memberRole) {
                if (!in_array($memberRole->getRole()->getId(), $roleIds)) {
                    $teamMember->removeMemberRole($memberRole);
                    $this->entityManager->remove($memberRole);
                }
            }

            $this->addNewRoles($teamMember, $roleIds);

            $this->entityManager->persist($teamMember);
            $this->entityManager->flush();

            $this->entityManager->lock($teamMember, \Doctrine\DBAL\LockMode::NONE);
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            $this->entityManager->lock($teamMember, \Doctrine\DBAL\LockMode::NONE);
            throw $e;
        }
    }


    private function addNewRoles(TeamMember $teamMember, array $roleIds): void
    {
        /** @var TeamMemberRole[] $roles */
        $roles = $this->entityManager->getRepository(TeamMemberRole::class)->findBy(['id' => $roleIds]);
        if (count($roles) === 0) {
            throw new \Exception("At least one role is required");
        }

        $existingRoleIds = array_map(function (MemberRole $memberRole) {
            return $memberRole->getRole()->getId();
        }, $teamMember->getMemberRoles()->toArray());

        foreach ($roles as $role) {
            if (in_array($role->getId(), $existingRoleIds)) {
                continue;
            }

            $memberRole = new MemberRole();
            $memberRole->setRole($role);
            $memberRole->setTeamMember($teamMember);
            $this->entityManager->persist($memberRole);
        }
    }


    public function createTeam(string $teamName, array $membersWithRoles): void
    {
        try {
            $this->entityManager->beginTransaction();
            $team = new Team($teamName);
            $this->entityManager->persist($team);

            /** @var TeamMember[] $teamMembers */
            $teamMembers = $this->entityManager->getRepository(TeamMember::class)->findBy(['id' => array_keys($membersWithRoles)]);

            /** @var TeamMemberRole[] $teamMemberRoles */
            $teamMemberRoles = $this->entityManager->getRepository(TeamMemberRole::class)->findBy(['id' => array_values($membersWithRoles)]);

            foreach ($teamMembers as $teamMember) {
                $membership = new TeamMembership();
                $membership->setMember($teamMember);
                $membership->setTeam($team);

                $role = array_filter($teamMemberRoles, function (TeamMemberRole $teamMemberRole) use ($teamMember, $membersWithRoles) {
                    return $teamMemberRole->getId() === $membersWithRoles[$teamMember->getId()];
                });
                $membership->setRole(reset($role));

                $this->entityManager->persist($membership);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    public function editTeam(Team $team, string $teamName, array $membersWithRoles): void
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->lock($team, \Doctrine\DBAL\LockMode::PESSIMISTIC_WRITE);

            $team->setTeamName($teamName);

            $memberships = $team->getMemberships();
            foreach ($memberships as $membership) {
                $team->removeMembership($membership);
                $this->entityManager->remove($membership);
            }

            $this->entityManager->flush();

            /** @var TeamMember[] $teamMembers */
            $teamMembers = $this->entityManager->getRepository(TeamMember::class)->findBy(['id' => array_keys($membersWithRoles)]);

            /** @var TeamMemberRole[] $teamMemberRoles */
            $teamMemberRoles = $this->entityManager->getRepository(TeamMemberRole::class)->findBy(['id' => array_values($membersWithRoles)]);

            foreach ($teamMembers as $teamMember) {
                $membership = new TeamMembership();
                $membership->setMember($teamMember);
                $membership->setTeam($team);

                $role = array_filter($teamMemberRoles, function (TeamMemberRole $teamMemberRole) use ($teamMember, $membersWithRoles) {
                    return $teamMemberRole->getId() === $membersWithRoles[$teamMember->getId()];
                });
                $membership->setRole(reset($role));

                $this->entityManager->persist($membership);
            }

            $this->entityManager->persist($team);
            $this->entityManager->flush();

            $this->entityManager->lock($team, \Doctrine\DBAL\LockMode::NONE);
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            $this->entityManager->lock($team, \Doctrine\DBAL\LockMode::NONE);
            throw $e;
        }
    }
}
