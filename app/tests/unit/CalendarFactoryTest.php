<?php

namespace unit;

use App\Services\Calendar\CalendarFactory;
use App\Services\Calendar\ICalendarSourceStorage;
use ICal\ICal;
use PHPUnit\Framework\TestCase;

class CalendarFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $mockStorage = $this->createMock(ICalendarSourceStorage::class);
        $mockStorage->method('getCalendarData')
            ->willReturn('data');

        $factory = new CalendarFactory();
        $calendar = $factory->create($mockStorage);
        $this->assertInstanceOf(ICal::class, $calendar);
    }
}