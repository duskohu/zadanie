SET SESSION sql_mode = CONCAT(@@GLOBAL.sql_mode, ',STRICT_ALL_TABLES');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `id`       int(11)      NOT NULL AUTO_INCREMENT,
    `password` varchar(255) NOT NULL,
    `email`    varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    KEY `email_password` (`email`, `password`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`
(
    `id`        int(11)                                                              NOT NULL AUTO_INCREMENT,
    `role_name` ENUM ('racer', 'technician', 'manager', 'co-driver', 'photographer') NOT NULL,
    `min_count` int(11)                                                              NOT NULL,
    `max_count` int(11)                                                              NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_role_name` (`role_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `roles` (`role_name`, `min_count`, `max_count`)
VALUES ('racer', 1, 3);
INSERT INTO `roles` (`role_name`, `min_count`, `max_count`)
VALUES ('technician', 1, 2);
INSERT INTO `roles` (`role_name`, `min_count`, `max_count`)
VALUES ('manager', 1, 1);
INSERT INTO `roles` (`role_name`, `min_count`, `max_count`)
VALUES ('co-driver', 1, 3);
INSERT INTO `roles` (`role_name`, `min_count`, `max_count`)
VALUES ('photographer', 0, 1);

DROP TABLE IF EXISTS `team_members`;
CREATE TABLE `team_members`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `name`    varchar(255) NOT NULL,
    `surname` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `member_roles`;
CREATE TABLE `member_roles`
(
    `member_id` int(11) NOT NULL,
    `role_id`   int(11) NOT NULL,
    FOREIGN KEY (`member_id`) REFERENCES `team_members` (`id`),
    FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `team_name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `team_memberships`;
CREATE TABLE `team_memberships`
(
    `team_id`   int(11) NOT NULL,
    `member_id` int(11) NOT NULL,
    `role_id`   int(11) NOT NULL,
    FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`),
    FOREIGN KEY (`member_id`) REFERENCES `team_members` (`id`),
    FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
    PRIMARY KEY (`team_id`, `member_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `calendar_events`;
CREATE TABLE `calendar_events`
(
    `id`            INT AUTO_INCREMENT PRIMARY KEY,
    `summary`       VARCHAR(255),
    `dtstart`       VARCHAR(255),
    `dtend`         VARCHAR(255),
    `uid`           VARCHAR(255),
    `last_modified` VARCHAR(255),
    INDEX (`uid`),
    INDEX (`dtstart`, `dtend`)
) ENGINE = InnoDB;

