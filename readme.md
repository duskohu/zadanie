## Prepare dev environment
```bash
./.build/prepare-dev.sh
docker compose up --build
```
go to http://localhost:999/

## Run tests
```bash
docker compose -f docker-compose.testing.yaml run phpunit
```